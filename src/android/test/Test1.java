/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author lendle
 */
public class Test1 extends CordovaPlugin {

    private static MediaPlayer mp = null;
    private static AudioManager am = null;
    private int originalVolume = -1;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if ("send".equals(action)) {
            executeBluetoothSend(args.getString(0), args.getString(1), callbackContext);
        } else if ("receive".equals(action)) {
            executeBluetoothReceive(args.getString(0), args.getString(1), callbackContext);
        } else if ("dial".equals(action)) {
            dial(args.getString(0), callbackContext);
        } else if ("registerPhoneListener".equals(action)) {
            registerPhoneListener(callbackContext);
        } else if ("playAudio".equals(action)) {
            playAudio(callbackContext);
        } else if ("stopPlayAudio".equals(action)) {
            stopPlayAudio(callbackContext);
        } else if ("startDiscovery".equals(action)) {
            startDiscovery(callbackContext);
        } else if ("startLEDiscovery".equals(action)) {
            startLEDiscovery(callbackContext);
        }
        return true;
    }

    protected boolean executeBluetoothSend(String uuid, String message, final CallbackContext callbackContext) throws JSONException {
        Log.w("test.test1", "execute");
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        StringBuffer buffer = new StringBuffer();
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                Log.i("test.test1", device.getName() + ":" + device.getAddress());
                buffer.append(device.getName() + ":" + device.getAddress() + ":" + device.getUuids()[0]);
            }
        }
        if (pairedDevices.size() > 0) {
            BluetoothDevice device = pairedDevices.toArray(new BluetoothDevice[0])[0];
            try {
                for (ParcelUuid _uuid : device.getUuids()) {
                    Log.i("test.test1", _uuid.toString());
                    buffer.append("[" + _uuid.toString() + "]");
                }
                //BluetoothSocket socket=device.createRfcommSocketToServiceRecord(device.getUuids()[0].getUuid());
                BluetoothSocket socket = device.createRfcommSocketToServiceRecord(UUID.fromString(uuid));

                //buffer.append(socket);
                socket.connect();
                OutputStream outputStream = socket.getOutputStream();
                outputStream.write((message + "\r\n").getBytes());
                outputStream.close();
                //buffer.append(":output finished");
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
                buffer.append(ex);
            }
        }
        callbackContext.success("finished: " + buffer);
        Log.w("test.test1", "execute");
        //callbackContext.success("finished");
        return true;
    }

    protected boolean executeBluetoothReceive(String name, String uuid, final CallbackContext callbackContext) throws JSONException {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            Log.w("test.test1", "opening server......" + uuid);
            final BluetoothServerSocket server = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(name, UUID.fromString(uuid));

            this.cordova.getThreadPool().execute(new Runnable() {

                @Override
                public void run() {
                    try {
                        Log.w("test.test1", "waiting messages......");
                        BluetoothSocket socket = server.accept();
                        InputStream input = socket.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                        String message = reader.readLine();
                        Log.w("test.test1", "message: " + message);
                        server.close();
                        //String message="test";
                        callbackContext.success("message: " + message);
                    } catch (IOException ex) {
                        Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    protected void dial(String number, final CallbackContext callbackContext) {
        Intent dial = new Intent();
        dial.setAction("android.intent.action.CALL");
        dial.setData(Uri.parse("tel:" + number));
        this.cordova.getActivity().startActivity(dial);
    }

    protected void registerPhoneListener(final CallbackContext callbackContext) {
        TelephonyManager telephonyManager = (TelephonyManager) this.cordova.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneStateListener() {

            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber); //To change body of generated methods, choose Tools | Templates.
                String stateString = null;
                switch (state) {
                    case TelephonyManager.CALL_STATE_IDLE:
                        stateString = "Idle";
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        stateString = "Off Hook";
                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        stateString = "Ringing";
                        break;
                };
                Log.w("test.test1", "call state: " + stateString);
            }

        }, PhoneStateListener.LISTEN_CALL_STATE);
        Log.w("test.test1", "registered");
    }

    protected void playAudio(final CallbackContext callbackContext) {
        try {
            am = (AudioManager) this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
            am.setSpeakerphoneOn(true);
            originalVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);
            am.setStreamVolume(AudioManager.STREAM_MUSIC,
                    am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    -1);
            mp = new MediaPlayer();
            mp.setDataSource("http://freedownloads.last.fm/download/569264057/Get%2BGot.mp3");
            mp.prepare();
            mp.start();
            mp.setOnCompletionListener(new OnCompletionListener() {

                @Override

                public void onCompletion(MediaPlayer mp) {

                    mp.release();

                }

            });
        } catch (Exception ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    protected void stopPlayAudio(final CallbackContext callbackContext) {
        try {
            am.setSpeakerphoneOn(false);
            if (mp != null) {
                mp.release();
            }
            if (am != null) {
                am.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, -1);
            }

        } catch (Exception ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    protected void startDiscovery(final CallbackContext callbackContext) {
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothDevice.ACTION_UUID);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            final List<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
            this.cordova.getActivity().registerReceiver(new android.content.BroadcastReceiver() {

                @Override
                public void onReceive(Context cntxt, Intent intent) {
                    String action = intent.getAction();
                    // When discovery finds a device
                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        // Get the BluetoothDevice object from the Intent
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        devices.add(device);
                        // Add the name and address to an array adapter to show in a ListView
                        //device.fetchUuidsWithSdp();
                        Log.i("test.test1", device.getName() + ":" + device.getAddress());
                        Parcelable[] uuidExtra = device.getUuids();
                        for (Parcelable uuid : uuidExtra) {
                            Log.i("test.test1", "\t" + device.getName() + ":" + uuid.toString());
                        }
                    } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                        Log.i("test.test1", "finished discovery");
                        while (devices != null) {
                            BluetoothDevice device = devices.remove(0);
                            device.fetchUuidsWithSdp();
                        }
                    } else if (BluetoothDevice.ACTION_UUID.equals(action)) {
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        Parcelable[] uuidExtra = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
                        for (Parcelable uuid : uuidExtra) {
                            Log.i("test.test1", device.getName() + ":" + uuid.toString());
                        }
                    }
                }

            }, filter);
            BluetoothAdapter.getDefaultAdapter().startDiscovery();
            Log.w("test.test1", "startDiscovery");
        } catch (Exception ex) {
            Logger.getLogger(Test1.class.getName()).log(Level.SEVERE, null, ex);
            Log.w("test.test1", ex.getMessage());
        }
    }

    protected void startLEDiscovery(final CallbackContext callbackContext) {
        final BluetoothManager bluetoothManager
                = (BluetoothManager) this.cordova.getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetooth = bluetoothManager.getAdapter();
        Log.i("test.test1", "startLEDiscovery");
        bluetooth.startLeScan(new LeScanCallback() {

            @Override
            public void onLeScan(BluetoothDevice bd, int i, byte[] bytes) {
                Log.i("test.test1", "device="+bd.getAddress());
                BluetoothGatt gatt=bd.connectGatt(cordova.getActivity(), true, new BluetoothGattCallback(){});
                List<BluetoothGattService> services=gatt.getServices();
                for(BluetoothGattService service : services){
                    Log.i("test.test1", "\tservice="+service.getUuid());
                    List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();
                    Log.i("test.test1", "\tgattCharacteristics="+gattCharacteristics);
                    for(BluetoothGattCharacteristic c : gattCharacteristics){
                        Log.i("test.test1", "\tcharacteristics="+c);
                    }
                }
                gatt.close();
            }
        });
    }
}
